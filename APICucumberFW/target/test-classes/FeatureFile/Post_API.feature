Feature: Trigger the POST API with required parameters

@Post_API
Scenario: Trigger the API request with valid requestbody parameters
Given Enter NAME and JOB in request body
When Send the request with payload
Then Validate status code 
And validate response body parameters

@Post_API
Scenario Outline: Test Post API with multiple test data
Given Enter "<NAME>" and "<JOB>" in request body
When Send the request with payload
Then Validate status code 
And validate response body parameters

Examples:
    |NAME|JOB|
    |SUCHITA|TESTER|
    |ANVIT|DEV|
    |ABHI|BA|