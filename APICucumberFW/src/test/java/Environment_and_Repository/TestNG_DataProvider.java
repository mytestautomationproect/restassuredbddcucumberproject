package Environment_and_Repository;

import org.testng.annotations.DataProvider;

public class TestNG_DataProvider {

	@DataProvider()
	public Object[][] requestBody() {
		return new Object[][] {

				{ "morpheus", "Leader" }, { "Brian", "Developer" } };
	}

}
